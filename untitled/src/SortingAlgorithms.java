import java.util.Arrays;

 public class SortingAlgorithms {

    public static void main(String[] args) {

        int[] a = {21, 4, 0,25/2, 9, -1, 8};
        System.out.println("Array: " + Arrays.toString(a));
        System.out.println("Selection Sort: " + Arrays.toString(selectionSort(Arrays.copyOf(a, a.length))));
        System.out.println("Bubble Sort: " + Arrays.toString(bubbleSort(Arrays.copyOf(a, a.length))));
        System.out.println("Insertion Sort: " + Arrays.toString(insertionSort(Arrays.copyOf(a, a.length))));
    }

    public static int[] selectionSort(int[] a) {
        int pos;
        int temp;
        for(int i = a.length-1;i>=1; i--) {
            pos = i;
            for (int j = 0; j <i; j++) {
                if (a[j] > a[pos])
                    pos = j;
            }
            temp=a[pos];
            a[pos]=a[i];
            a[i]=temp;
        }
        return a;
    }

     public static int[] bubbleSort(int[] a) {
         for (int i = 1; i < a.length; i++) {
             boolean isSorted = true;
             for (int j = 0; j < a.length - i; j++) {
                 if (a[j] > a[j+1]) {
                     swap(a, j, j+1);
                     isSorted = false;
                 }
             }
             if (isSorted)
                 return a;
         }
         return a;
     }

     public static void swap(int[] a, int x, int y) {
        int temp = a[x];
        a[x] = a[y];
        a[y] = temp;
    }

    public static int[] insertionSort(int[] a) {
        for (int i = 1; i < a.length; i++) {
            int value = a[i];
            int j;
            for (j = i-1; j >= 0 && a[j] > value; j--)
                a[j+1] = a[j];
            a[j+1] = value;
        }
        return a;
    }}

